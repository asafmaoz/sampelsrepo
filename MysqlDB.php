<?php
/**
 * Creator: Asaf Maoz
 * Class for connecting to DB using PDO
 *
 * Singleton class
 * Setting up connection and executing queries
 */

class Mysqldb{

    protected static $instance;
    private $address = 'mysql:host=localhost;';
    private $dbName = 'dbname=your_db_name';
    private $userName = 'root';
    private $pass = '';
    protected $db;

    private function __construct(){
        try{
            $this->db = new PDO("$this->address.$this->dbName",$this->userName,$this->pass,array(PDO::ATTR_PERSISTENT => true));
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            register_shutdown_function(array($this, 'close_connection'));
        }
        catch(PDOException $e){
            $errorMessage = $e->getMessage();
            $this->error_handeling('Error connection: '.$errorMessage);
        }
        self::$instance = $this;
    }
    public static function get_db_connection(){
        if(!self::$instance){
            self::$instance = new Mysqldb;
        }
        return self::$instance;
    }

    public function close_connection(){
        $this->db = null;
    }
    public function __destruct(){
        //close connection
        $this->db = null;
    }
    private function error_handeling($errorMessage,$query='')
    {
        file_put_contents('PDOErrors.txt', $errorMessage.' '.$query.' '.date('m/d/Y h:i:s a', time()), FILE_APPEND);
        $this->db = null;
        die("<pre>\n Error, Please try again... \n</pre>");

    }


    public function get_last_inserted_id($columnName){
        return $this->db->lastInsertId($columnName);
    }
    /**
     * @param $query
     * @param array $valuesArr
     * @return PDOStatement
     */
    public function run_mysql_query($query,$valuesArr=array()){
        try{
            $preparedQuery = $this->db->prepare($query,array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            if(empty($valuesArr))
                $preparedQuery->execute();
            else
                $preparedQuery->execute($valuesArr);
        }
        catch(PDOException $e)
        {
            $query .= implode(', ',$valuesArr);
            $this->error_handeling($e->getMessage(),$query);
        }

        return $preparedQuery;
    }

    /**
     * Run a query and get one row result
     * @param $query
     * @param array $valuesArr
     * @return mixed
     */
    public function get_query_row($query,$valuesArr=array()){
        try{
            $preparedQuery = $this->db->prepare($query,array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            if(empty($valuesArr))
                $preparedQuery->execute();
            else
                $preparedQuery->execute($valuesArr);
        }catch(PDOException $e){

            $query .= implode(', ',$valuesArr);
            $this->error_handeling($e->getMessage(),$query);
        }
        return($preparedQuery->fetch(PDO::FETCH_ASSOC));
    }

    /**
     * @param $query
     * @param array $valuesArr
     * @return array
     */
    public function get_query_results($query,$valuesArr=array()){
        $results = $this->run_mysql_query($query,$valuesArr);
        return($results->fetchAll(PDO::FETCH_ASSOC));
    }

    /**
     * Run INSERT/UPDATE multiple times with different data
     * @param $query
     * @param array $data
     */
    public function run_query_multi_diff_data($query,$data=array())
    {
        $preparedQuery = $this->db->prepare($query,array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        foreach($data as $queryVars){
            $preparedQuery->execute($queryVars);
        }
        return;
    }


}