/**
 * Creating the search element
 * Can set specific element attributes
 * @constructor
 */
function JS_searchAutoComplete(){
    this.Element = {
        minimumChar : 1,//Minimum characters needed
        hasTabs : false,//Is it the kind of search that has tabs
        hasDefaultText : true,//Is there a default text set
        selectedSpecialId : 0
    };
}
//Creating prototype element
//Setting attributes for all instances
JS_searchAutoComplete.prototype = {
    rowNumber     : 0,
    lastRowNumber : 1,
    searchText    : '',
    allowdReset   : true,
    allowdClosePopUp : true,
    allowdKeys : false,
    allowdForm : false,
    searchDeafaultVale : '',
    searchTimeOutRunning : false,
    arrowfocus:'',
    current_tab : 'All',
    previousTab : 'All',
    resultsData:'',
    firstRowId: 0,
    dynamicVar : '',
    specialsData : undefined,
    specialsDefaultText : undefined,
    searchType: '',
    porTId: undefined,

//General functions
    clearInputField : function(Obj) {

        if (searchSelfElement.searchDeafaultVale == ''){ ///set default value
            searchSelfElement.searchDeafaultVale = $(Obj).val();
        }
        if(searchSelfElement.searchDeafaultVale ==  $(Obj).val()){//check if need to clear
            $('#'+this.Element.searchTextInputId).val('');
            searchSelfElement.allowdReset = true;
            searchSelfElement.allowdKeys = false;
            searchSelfElement.allowdForm = false;
        }
    },

    markSelectedRow : function(){
        $('#'+this.Element.resultsInnerContainerId+' .row').removeClass('hoverSearch');
        $('#'+this.Element.mainResultRowIdPrefix+searchSelfElement.rowNumber).addClass('hoverSearch');

    },
    setSelectedRowNumber : function(row) {
        searchSelfElement.allowdForm = true;
        this.lastRowNumber = searchSelfElement.rowNumber;
        searchSelfElement.rowNumber = row;
    },
    changeCurrentTabDisplay : function(tabId){

        $('#'+this.Element.tabsPrefix+tabId).addClass('selected');
        $('#'+this.Element.tabsPrefix+tabId).siblings().each(function(){
            $(this).removeClass('selected');
        });
    },
    getSelectedRowElement : function(obj) {
        return $('#'+obj.Element.mainResultRowIdPrefix + searchSelfElement.rowNumber);
    },
    closeAutocomplete : function(obj){
        obj.Element.selectedSpecialId = 0;
        searchSelfElement.previousTab = 'All';
        $('#'+this.Element.resultsInnerContainerId).scrollTop(0);
        $('#'+this.Element.specialsScrollDiv).scrollTop(0);
        $('#'+obj.Element.specialeWrapperId).addClass('displayNone');
        var defaultText = this.specialsDefaultText;
        if(this.specialsDefaultText == undefined){
            defaultText = this.Element.specialsDefaultText;
        }
        $('#'+obj.Element.tabsPrefix+'Specials').html('<a href="javascript:void(0);">'+ defaultText +'</a>');

        $('#'+obj.Element.resultsOuterContainerId).addClass('displayNone');
        $('#'+obj.Element.resultsInnerContainerId).html('');

    },
    closeSearchResultBox : function(){
        $('#'+this.Element.resultsOuterContainerId).addClass('displayNone');
        searchSelfElement.closeAutocomplete.call(this,this);

    },
    getResultsNumber : function(){
        return  $('#'+this.Element.resultsInnerContainerId).find("table tr").length;
    },
//End - General functions
//Key functionality
    keyCodeUp : function(){
        searchSelfElement.rowNumber--;
        var total_rows = searchSelfElement.getResultsNumber.call(this);
        if(searchSelfElement.rowNumber == -1 || isNaN(searchSelfElement.rowNumber) )
        {
            $('#'+this.Element.resultsInnerContainerId+' .row').removeClass('hoverSearch');
            $('#'+this.Element.formElementId).attr('onsubmit', 'return false;');
            searchSelfElement.arrowfocus = "text";
            $('#'+this.Element.searchTextInputId).focus();
            searchSelfElement.rowNumber = -1;
            $('#'+this.Element.searchTextInputId).val(searchSelfElement.searchText);
            searchSelfElement.allowdKeys = true;
        }
        if( (total_rows - searchSelfElement.rowNumber) >7){
            $('#'+this.Element.resultsInnerContainerId).scrollTop(($('#'+this.Element.resultsInnerContainerId).scrollTop())-22 );//22 is row height
        }

    },
    keyCodeDown : function(){

        if(searchSelfElement.rowNumber === undefined)
            searchSelfElement.rowNumber = 0;

        searchSelfElement.arrowfocus = "tabs";
        var total_rows = searchSelfElement.getResultsNumber.call(this);
        if(total_rows == searchSelfElement.rowNumber)
        {
            return;
        }

        $('#'+this.Element.formElementId).attr('onsubmit', 'return true;');
        searchSelfElement.rowNumber++;
        if(searchSelfElement.rowNumber > 7)
            $('#'+this.Element.resultsInnerContainerId).scrollTop(($('#'+this.Element.resultsInnerContainerId).scrollTop())+22 );//22 is row height
        else
            $('#'+this.Element.resultsInnerContainerId).scrollTop(0);
    },
    keyFunctunality : function(evt, keyCode){

        searchSelfElement.allowdForm = false;
        searchSelfElement.lastRowNumber = searchSelfElement.rowNumber;
        var showSymbol = true;
        if (keyCode == 40){ // down
            searchSelfElement.keyCodeDown.call(this);
        }
        else if (keyCode == 38){ //up
            searchSelfElement.keyCodeUp.call(this);
        }
        else if(keyCode == 39 && searchSelfElement.arrowfocus == "tabs" && (this.Element.hasTabs))//arrow right
        {
            var tab = searchSelfElement.getTheNextTab.call(this,1);//send the side (1 = right)
            if(tab == ''){
                showSymbol = false;
            }
            else{
                searchSelfElement.rowNumber = 0;
            }
            searchSelfElement.ChangeTabOnClick.call(this,tab);
        }
        else if(keyCode == 37 && searchSelfElement.arrowfocus == "tabs" && (this.Element.hasTabs))//arrow left
        {
            var tab = searchSelfElement.getTheNextTab.call(this,0);//send the side (2 = left)
            if(tab == ''){
                showSymbol = false;
            }else{
                searchSelfElement.rowNumber = 0;
            }
            searchSelfElement.ChangeTabOnClick.call(this,tab);
        }

        var selectedRow = searchSelfElement.getSelectedRowElement(this);
        if(selectedRow.length > 0 && showSymbol){
            var symbol = searchSelfElement.executeGetSymbol.call(this,selectedRow);
            searchSelfElement.symbolAndAction.call(this,symbol, selectedRow,$('#'+this.Element.searchTextInputId),false);
            searchSelfElement.markSelectedRow.call(this);
        }


    },

//End - Key functionality

//Show results and rows action
    displayAndSetSearchResultBox : function(){

        searchSelfElement.allowdForm = true;
        searchSelfElement.arrowfocus = 'tabs';

        if(searchSelfElement.firstRowId === 'no_results'){
            searchSelfElement.allowdForm = true;
            $('#'+this.Element.formElementId).attr('onsubmit', 'return true;');
        }
        else if(searchSelfElement.firstRowId === 'no_results'){
            searchSelfElement.allowdForm = false;
            $('#'+this.Element.formElementId).attr('onsubmit', 'return false;');
        }
        else{
            $('#'+this.Element.formElementId).attr('onsubmit', 'return searchSelfElement.allowdForm;');
        }
        $('#'+this.Element.formElementId).attr('action', $('#'+this.Element.mainResultRowIdPrefix + searchSelfElement.firstRowId + ' .symbolName').attr('link'));
        //add pairId
        this.Element.porTId = $('#'+this.Element.mainResultRowIdPrefix + searchSelfElement.firstRowId + ' .symbolName').attr('pairid');
        var obj = this;
        if(this.Element.hasTabs)
            searchSelfElement.bindTabs.call(obj);

        searchSelfElement.executeBindResults.call(obj);
        $('#'+this.Element.resultsOuterContainerId).removeClass('displayNone');
    },

//End - Show results and rows action

//Send ajax call
    validateForSearch : function(evt,Obj) {
        if(evt.keyCode != 38 && evt.keyCode != 40 && evt.keyCode != 37 && evt.keyCode != 39) {
            searchSelfElement.searchText = $(Obj).val();
        }
        if((searchSelfElement.searchText.length > 0) && (evt.keyCode == 38 || evt.keyCode == 40 || evt.keyCode == 37 || evt.keyCode == 39) && searchSelfElement.allowdKeys)
        {
            searchSelfElement.keyFunctunality.call(this,evt,evt.keyCode);
        }
        else if((evt.keyCode != 13) && (searchSelfElement.searchText.length >= this.Element.minimumChar) && (!searchSelfElement.searchTimeOutRunning))
        {
            searchSelfElement.allowdReset = false;
            searchSelfElement.checkIfTimedOut.call(this);
        }
        else if(searchSelfElement.searchText.length == 0)
        {
            searchSelfElement.closeSearchResultBox.call(this);
        }
    },
    checkIfTimedOut : function(){

        if(!searchSelfElement.searchTimeOutRunning) {
            var that = this;
            searchSelfElement.searchTimeOutRunning = true;
            that.timer = setTimeout(function() { searchSelfElement.executeAutoCompleteSearch.call(that); },400);
        }
    },
    executeAutoCompleteSearch : function(){

        if(searchSelfElement.searchText == ''){
            searchSelfElement.searchTimeOutRunning = false;
            return;
        }

        var se = $('#'+ this.Element.formElementId +' > .searchGlassIcon');
        se.removeClass('searchGlassIcon newSiteIconsSprite');
        se.addClass('loading');

        var obj = this;
        searchSelfElement.allowdForm = false;
        if(obj.Element.selectedSpecialId == '' || isNaN(parseInt(obj.Element.selectedSpecialId)) )
            obj.Element.selectedSpecialId = 0;


        $.post(this.Element.url,
            {
                search_text    	  : searchSelfElement.searchText,
                term    	  : searchSelfElement.searchText,
                extraParameters   : searchSelfElement.extraParameters,
                special_id		  : obj.Element.selectedSpecialId
            },
            function(data) {

                searchSelfElement.allowdKeys = false;
                searchSelfElement.allowdForm = false;
                searchSelfElement.resultsData = data;

                if(data != '' && data != null)
                {
                    searchSelfElement.allowdKeys = true;
                    searchSelfElement.arrowfocus = 'text';
                }
                else{
                    searchSelfElement.closeSearchResultBox.call(obj);
                    se.removeClass('loading');
                    se.addClass('searchGlassIcon newSiteIconsSprite');
                    return;
                }

                searchSelfElement.executeBuildResultsBox.call(obj);
                searchSelfElement.displayAndSetSearchResultBox.call(obj);

                se.removeClass('loading');
                se.addClass('searchGlassIcon newSiteIconsSprite');

            }, "json"
        );

        searchSelfElement.searchTimeOutRunning = false;
    },
//End - Send ajax call

//Default, bind searchText input field
    inputFieldBind : function(Obj){
        var that = this;

        Obj.bind(
            'focus' , function() {
                searchSelfElement.clearInputField.call(that, Obj);
                $('#'+ this.id + 'Container').addClass('InputSelectedClass');
                searchSelfElement.rowNumber = 0;
            });
        Obj.bind(
            'keyup' , function(event) {
                searchSelfElement.allowdKeys=true;
                searchSelfElement.validateForSearch.call(that, event, Obj);
            });
        $('#'+that.Element.searchTextInputId).bind(
            'focusout' , function() {
                $(document).click( function (evt) {
                    if(
                        ($(evt.target).parents('#'+that.Element.resultsInnerContainerId).length < 1)
                            &&
                            (evt.target.id != that.Element.resultsInnerContainerId )
                            &&
                            ($(evt.target).parents('#'+that.Element.searchTextInputId).length < 1)
                            &&
                            (evt.target.id != that.Element.searchTextInputId )
                            &&
                            ($(evt.target).parents('#'+that.Element.resultsOuterContainerId).length < 1)
                            &&
                            (evt.target.id != that.Element.resultsOuterContainerId )
                        ){
                        if( !(that.Element.hasDefaultText) )
                            $('#'+that.Element.searchTextInputId).val('');
                        else{
                            $('#'+that.Element.searchTextInputId).val(searchSelfElement.searchDeafaultVale);
                        }

                        $('#'+ that.Element.searchTextInputId + 'Container').removeClass('InputSelectedClass');
                        searchSelfElement.closeAutocomplete.call(that,that);
                    }
                });

            });


    },
//End - Default, bind searchText input field

//Tabs related functions
    //Coming here from clicking on tab or moving with arrows
    //tab_id is the tab to switch too
    ChangeTabOnClick : function (tab_id) {
        tab_id = tab_id.replace(this.Element.tabsPrefix,"");

        var obj = this;
        var changeTab = true;
        if(tab_id == ''){
            changeTab = false;
            tab_id = searchSelfElement.previousTab;
        }else{
                $('#'+obj.Element.specialsWrapperId).addClass('displayNone');
            }
    },
    bindSpecials : function(){
        var obj = this;
        $('#'+obj.Element.specialTableId+' '+obj.Element.specialsType).each(function(){
            if(obj.Element.specialRowPrefix+obj.Element.selectedSpecialId != this.id)
                $(this).removeClass('selected');
            $(this).unbind();

            $(this).click( function(){
                searchSelfElement.searchInspecialsId.call(obj,this.id);
            });
        });
    },
    //called from onClick of specials
    specialInSpecialsId : function(tdId){


    },
    bindTabs : function(){
        var obj = this;
        $('#'+obj.Element.tabsDivId+' '+obj.Element.tabsType).each(function(){
            $(this).unbind();
            $(this).click( function(){
                searchSelfElement.ChangeTabOnClick.call(obj,this.id);
            })
        });
    },
    //get the side (1 = right , 2 = left)
    getTheNextTab :  function (side)
    {
        if($('#'+this.Element.hiddenDocDirectionId).attr('rel') == 'rtl')
        {
            if(side == 0)
                side = 1;
            else
                side = 0;
        }
        var obj = this;
        var tabsArr = new Array();
        var tabId = '';
        var tabNameClean = '';
        var index = 0;

        $('#'+obj.Element.tabsDivId+' '+obj.Element.tabsType).each(function(){

            tabId = this.id;
            tabNameClean = tabId.replace(obj.Element.tabsPrefix , '');
            tabsArr[index] = tabNameClean;
            index++;

        });
        var nextTab = '';
        for(var i=0;i<tabsArr.length;i++){
            if(searchSelfElement.previousTab == tabsArr[i])
            {
                if(side == 0)
                    side = -1;
                nextTab = tabsArr[i+side];
            }
        }
        if(nextTab != '' && nextTab != undefined && nextTab != 'Specials')
            nextTab = obj.Element.tabsPrefix+nextTab;
        else{
            nextTab = '';
        }
        return nextTab;

    },
//End - tabs related functions

//Execute specific search functions
    executeBuildResultsBox : function(){
        var obj = this;
        var data = searchSelfElement.resultsData;
        obj.buildSearchResults(data);
    },
    executeBindResults : function(){
        var obj = this;
        $('#'+obj.Element.resultsInnerContainerId+' .row').each(function(){
            $(this).unbind();
            obj.bindResults(this);
        });
    },
    executeGetSymbol : function(selectedRowObj){
        return (this.getSymbol(selectedRowObj));
    }
//End - Execute specific search functions


};

var searchSelfElement = JS_searchAutoComplete.prototype;